def calculate_average(grades):
    return sum(grades) / len(grades)

def main():
    # Given list of dictionaries
    students = [
        {'name': 'John', 'grades': [85, 90, 78, 92, 88]},
        {'name': 'Alice', 'grades': [76, 82, 95, 88, 91]},
        {'name': 'Bob', 'grades': [90, 88, 84, 79, 95]},
        # Add more students as needed
    ]

    # 1. Calculate average grade for each student using list comprehension
    averages = [(student['name'], calculate_average(student['grades'])) for student in students]

    # 2. Identify students with an average grade above a certain threshold
    threshold = 75
    above_threshold = [student for student, avg in averages if avg > threshold]

    # 3. Create a new list of students with their names and average grades, sorted in descending order
    sorted_averages = sorted(averages, key=lambda x: x[1], reverse=True)

    # 4. Identify the student with the highest average grade
    highest_avg_student = max(averages, key=lambda x: x[1])

    # 5. Store results in a text file
    with open('student_grades_results.txt', 'w') as file:
        file.write("Average Grades:\n")
        for name, avg in averages:
            file.write(f"{name}: {avg:.2f}\n")

        file.write("\nStudents above the threshold:\n")
        for student in above_threshold:
            file.write(f"{student}\n")

        file.write("\nStudents sorted by average grade (descending order):\n")
        for name, avg in sorted_averages:
            file.write(f"{name}: {avg:.2f}\n")

        file.write("\nStudent with the highest average grade:\n")
        file.write(f"{highest_avg_student[0]}: {highest_avg_student[1]:.2f}")

    print("Results stored in student_grades_results.txt")

if __name__ == "__main__":
    main()
